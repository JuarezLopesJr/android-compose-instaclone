package com.example.instaclonecompose.screens

import android.net.Uri
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.OutlinedButton
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.BottomEnd
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import coil.annotation.ExperimentalCoilApi
import com.example.instaclonecompose.R
import com.example.instaclonecompose.model.NavParams
import com.example.instaclonecompose.model.PostData
import com.example.instaclonecompose.model.PostRow
import com.example.instaclonecompose.navigation.NavScreens
import com.example.instaclonecompose.ui.theme.CARD_DEFAULT_SIZE
import com.example.instaclonecompose.ui.theme.CARD_SMALL_BORDER
import com.example.instaclonecompose.ui.theme.DIVIDER_WIDTH
import com.example.instaclonecompose.ui.theme.LARGE_CARD_IMAGE_SIZE
import com.example.instaclonecompose.ui.theme.MEDIUM_PADDING
import com.example.instaclonecompose.ui.theme.ROW_HEIGHT
import com.example.instaclonecompose.ui.theme.SMALL_PADDING
import com.example.instaclonecompose.utils.CommonImage
import com.example.instaclonecompose.utils.CommonProgressSpinner
import com.example.instaclonecompose.utils.Constants
import com.example.instaclonecompose.utils.Constants.POSTS
import com.example.instaclonecompose.utils.UserImageCard
import com.example.instaclonecompose.utils.navigateTo
import com.example.instaclonecompose.viewmodel.MainViewModel

@ExperimentalCoilApi
@Composable
fun MyPostsScreen(
    navController: NavController,
    mainViewModel: MainViewModel = hiltViewModel()
) {
    val newPostImageLauncher =
        rememberLauncherForActivityResult(
            contract = ActivityResultContracts.GetContent()
        ) {
            it?.let { uri ->
                val encoded = Uri.encode(uri.toString())
                val route = NavScreens.NewPost.createRoute(imageUri = encoded)

                navController.navigate(route = route)
            }
        }

    val userData = mainViewModel.userData.value

    val isLoading = mainViewModel.inProgress.value

    val postLoading = mainViewModel.refreshPostProgress.value

    val posts = mainViewModel.posts.value

    val followers = mainViewModel.followers.value

    Column {
        Column(modifier = Modifier.weight(1f)) {
            Row {
                ProfileImage(
                    imageUrl = userData?.imageUrl,
                    onClick = {
                        newPostImageLauncher.launch("image/*")
                        navigateTo(navController = navController, screen = NavScreens.NewPost)
                    }
                )

                Text(
                    text = "${posts.size}\nposts", modifier = Modifier
                        .weight(1f)
                        .align(CenterVertically),
                    textAlign = TextAlign.Center,
                    color = MaterialTheme.colors.onSurface
                )

                Text(
                    text = "$followers\nfollowers", modifier = Modifier
                        .weight(1f)
                        .align(CenterVertically),
                    textAlign = TextAlign.Center,
                    color = MaterialTheme.colors.onSurface
                )

                Text(
                    text = "${userData?.following?.size ?: 0}\nfollowing", modifier = Modifier
                        .weight(1f)
                        .align(CenterVertically),
                    textAlign = TextAlign.Center,
                    color = MaterialTheme.colors.onSurface
                )
            }
            Column(modifier = Modifier.padding(all = SMALL_PADDING)) {
                val userNameDisplay =
                    if (userData?.userName == null) "" else "@${userData.userName}"

                Text(
                    text = userData?.name ?: "",
                    fontWeight = FontWeight.Bold,
                    color = MaterialTheme.colors.onSurface
                )

                Text(
                    text = userNameDisplay,
                    color = MaterialTheme.colors.onSurface
                )

                Text(
                    text = userData?.bio ?: "",
                    color = MaterialTheme.colors.onSurface
                )
            }

            OutlinedButton(
                onClick = {
                    navigateTo(
                        navController = navController,
                        screen = NavScreens.Profile
                    )
                }, modifier = Modifier
                    .padding(all = SMALL_PADDING)
                    .fillMaxWidth(),
                colors = ButtonDefaults
                    .buttonColors(backgroundColor = Color.Transparent),
                elevation = ButtonDefaults.elevation(
                    defaultElevation = 0.dp,
                    pressedElevation = 0.dp,
                    disabledElevation = 0.dp
                ),
                shape = RoundedCornerShape(percent = 10)
            ) {
                Text(
                    text = stringResource(R.string.edit_profile_btn),
                    color = MaterialTheme.colors.onSurface
                )
            }

            PostList(
                modifier = Modifier
                    .weight(1f)
                    .padding(all = DIVIDER_WIDTH)
                    .fillMaxSize(),
                isContextLoading = isLoading,
                postLoading = postLoading,
                posts = posts,
                onPostClick = {
                    navigateTo(
                        navController = navController,
                        screen = NavScreens.SinglePost,
                        NavParams(name = POSTS, value = it)
                    )
                }
            )
        }
        BottomNavigationMenu(
            selectedItem = BottomNavigationItem.POSTS,
            navController = navController
        )
    }

    if (isLoading) {
        CommonProgressSpinner()
    }
}

@ExperimentalCoilApi
@Composable
fun ProfileImage(imageUrl: String?, onClick: () -> Unit) {
    Box(
        modifier = Modifier
            .padding(top = MEDIUM_PADDING)
            .clickable { onClick.invoke() }
    ) {
        UserImageCard(
            modifier = Modifier
                .padding(all = SMALL_PADDING)
                .size(LARGE_CARD_IMAGE_SIZE),
            userImage = imageUrl
        )

        Card(
            shape = CircleShape, border = BorderStroke(
                width = CARD_SMALL_BORDER,
                color = Color.White
            ),
            modifier = Modifier
                .size(CARD_DEFAULT_SIZE)
                .align(alignment = BottomEnd)
                .padding(bottom = SMALL_PADDING, end = SMALL_PADDING)
        ) {
            Image(
                imageVector = Icons.Default.Add,
                contentDescription = null,
                modifier = Modifier.background(color = Color.Blue),
                colorFilter = ColorFilter.tint(color = Color.White)
            )
        }
    }
}

@ExperimentalCoilApi
@Composable
fun PostList(
    modifier: Modifier = Modifier,
    isContextLoading: Boolean,
    postLoading: Boolean,
    posts: List<PostData>,
    onPostClick: (PostData) -> Unit
) {
    when {
        postLoading -> CommonProgressSpinner()
        posts.isEmpty() -> {
            Column(
                modifier = modifier,
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center
            ) {
                if (!isContextLoading) Text(text = Constants.POSTS_UNAVAILABLE)
            }
        }
        else -> {
            LazyColumn(modifier = modifier) {
                val rows = arrayListOf<PostRow>()
                var currentRow = PostRow()
                rows.add(currentRow)

                for (post in posts) {
                    if (currentRow.isFull()) {
                        currentRow = PostRow()
                        rows.add(currentRow)
                    }

                    currentRow.add(post = post)
                }

                items(items = rows) {
                    PostsRow(item = it, onPostClick = onPostClick)
                }
            }
        }
    }
}

@ExperimentalCoilApi
@Composable
fun PostsRow(
    item: PostRow,
    onPostClick: (PostData) -> Unit
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .height(ROW_HEIGHT)
    ) {
        PostImage(
            modifier = Modifier
                .weight(1f)
                .clickable {
                    item.post1?.let { post ->
                        onPostClick(post)
                    }
                },
            imageUri = item.post1?.postImage
        )

        PostImage(
            modifier = Modifier
                .weight(1f)
                .clickable {
                    item.post2?.let { post ->
                        onPostClick(post)
                    }
                },
            imageUri = item.post2?.postImage
        )

        PostImage(
            modifier = Modifier
                .weight(1f)
                .clickable {
                    item.post3?.let { post ->
                        onPostClick(post)
                    }
                },
            imageUri = item.post3?.postImage
        )
    }
}

@ExperimentalCoilApi
@Composable
fun PostImage(
    modifier: Modifier = Modifier,
    imageUri: String?
) {
    Box(modifier = modifier) {
        var imageModifier = Modifier
            .padding(DIVIDER_WIDTH)
            .fillMaxSize()

        if (imageUri == null) {
            imageModifier = imageModifier.clickable(enabled = false) {}
        }

        CommonImage(
            data = imageUri,
            modifier = imageModifier,
            contentScale = ContentScale.Crop
        )
    }
}