package com.example.instaclonecompose.screens

import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.style.TextDecoration
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.example.instaclonecompose.R
import com.example.instaclonecompose.model.CommentData
import com.example.instaclonecompose.ui.theme.DIVIDER_WIDTH
import com.example.instaclonecompose.ui.theme.MEDIUM_PADDING
import com.example.instaclonecompose.ui.theme.SMALL_PADDING
import com.example.instaclonecompose.utils.CommonProgressSpinner
import com.example.instaclonecompose.utils.Constants.COMMENTS_ERROR
import com.example.instaclonecompose.viewmodel.MainViewModel

@ExperimentalComposeUiApi
@Composable
fun CommentsScreen(
    mainViewModel: MainViewModel = hiltViewModel(),
    navController: NavController,
    postId: String
) {
    var commentText by rememberSaveable { mutableStateOf("") }

    val keyboardController = LocalSoftwareKeyboardController.current

    val focusManager = LocalFocusManager.current

    val comments = mainViewModel.comments.value

    val commentProgress = mainViewModel.commentProgress.value

    Column(modifier = Modifier.fillMaxSize()) {

        when {
            commentProgress -> {
                Column(
                    modifier = Modifier.fillMaxWidth(),
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    CommonProgressSpinner()
                }
            }

            comments.isEmpty() -> {
                Column(modifier = Modifier.weight(1f)) {
                }
                Column(
                    modifier = Modifier.fillMaxWidth(),
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Text(text = COMMENTS_ERROR, color = MaterialTheme.colors.onSurface)
                }
            }

            else -> {
                LazyColumn(modifier = Modifier.weight(1f)) {
                    items(items = comments) { comment ->
                        CommentRow(comment)
                    }
                }
            }
        }

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(all = SMALL_PADDING)
        ) {
            TextField(
                value = commentText,
                onValueChange = { commentText = it },
                modifier = Modifier
                    .weight(1f)
                    .border(DIVIDER_WIDTH, Color.LightGray),
                colors = TextFieldDefaults.textFieldColors(
                    backgroundColor = Color.Transparent,
                    focusedIndicatorColor = Color.Transparent,
                    unfocusedIndicatorColor = Color.Transparent,
                    disabledIndicatorColor = Color.Transparent
                ),
                textStyle = TextStyle(
                    color = MaterialTheme.colors.onSurface,
                    textDecoration = TextDecoration.None
                ),
                keyboardOptions = KeyboardOptions(
                    imeAction = ImeAction.Done
                ),
                keyboardActions = KeyboardActions(
                    onDone = {
                        keyboardController?.hide()
                        focusManager.clearFocus()
                    }
                )
            )

            Button(
                onClick = {
                    mainViewModel.createComment(postId = postId, text = commentText)
                    commentText = ""
                    keyboardController?.hide()
                    focusManager.clearFocus()
                    navController.popBackStack()
                },
                modifier = Modifier.padding(start = SMALL_PADDING, top = MEDIUM_PADDING)
            ) {
                Text(
                    text = stringResource(R.string.comment_btn_title),
                    color = MaterialTheme.colors.onSurface
                )
            }
        }
    }
}

@Composable
fun CommentRow(comment: CommentData) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(SMALL_PADDING)
    ) {
        Text(
            text = comment.userName ?: "",
            fontWeight = FontWeight.Bold,
            color = MaterialTheme.colors.onSurface
        )
        Text(
            text = comment.text ?: "",
            modifier = Modifier.padding(start = SMALL_PADDING),
            color = MaterialTheme.colors.onSurface
        )
    }
}