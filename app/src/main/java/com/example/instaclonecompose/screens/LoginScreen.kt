package com.example.instaclonecompose.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Button
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Email
import androidx.compose.material.icons.filled.Lock
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextDecoration
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.example.instaclonecompose.R
import com.example.instaclonecompose.navigation.NavScreens
import com.example.instaclonecompose.ui.theme.IMAGE_LARGE_WIDTH
import com.example.instaclonecompose.ui.theme.LARGE_TEXT_SIZE
import com.example.instaclonecompose.ui.theme.MEDIUM_PADDING
import com.example.instaclonecompose.ui.theme.SMALL_PADDING
import com.example.instaclonecompose.utils.BackButtonHandler
import com.example.instaclonecompose.utils.CheckSignedIn
import com.example.instaclonecompose.utils.CommonProgressSpinner
import com.example.instaclonecompose.utils.navigateTo
import com.example.instaclonecompose.viewmodel.MainViewModel

@ExperimentalComposeUiApi
@Composable
fun LoginScreen(
    navController: NavController,
    mainViewModel: MainViewModel = hiltViewModel()
) {

    CheckSignedIn(
        navController = navController,
        mainViewModel = mainViewModel
    )

    val emailState = remember {
        mutableStateOf(TextFieldValue())
    }

    val passwordState = remember {
        mutableStateOf(TextFieldValue())
    }

    val keyboardController = LocalSoftwareKeyboardController.current
    val focusManager = LocalFocusManager.current
    val isLoading = mainViewModel.inProgress.value

    // TODO("fix back button navigation")
    BackButtonHandler()

    Box(modifier = Modifier.fillMaxSize()) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentHeight()
                .verticalScroll(rememberScrollState()),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {

            Image(
                modifier = Modifier
                    .width(IMAGE_LARGE_WIDTH)
                    .padding(top = MEDIUM_PADDING)
                    .padding(all = SMALL_PADDING),
                painter = painterResource(id = R.drawable.ig_logo),
                contentDescription = stringResource(R.string.logo_image_description)
            )

            Text(
                text = stringResource(R.string.login_title),
                modifier = Modifier.padding(all = SMALL_PADDING),
                fontSize = LARGE_TEXT_SIZE,
                fontFamily = FontFamily.SansSerif,
                color = MaterialTheme.colors.onSurface
            )

            OutlinedTextField(
                value = emailState.value,
                onValueChange = { emailState.value = it },
                modifier = Modifier.padding(all = SMALL_PADDING),
                label = {
                    Text(text = stringResource(R.string.email_label))
                },
                colors = TextFieldDefaults.textFieldColors(),
                trailingIcon = {
                    Icon(
                        imageVector = Icons.Default.Email,
                        contentDescription = null,
                        tint = MaterialTheme.colors.onSurface
                    )
                },
                keyboardOptions = KeyboardOptions(
                    imeAction = ImeAction.Next
                ),
                keyboardActions = KeyboardActions(
                    onNext = {
                        focusManager.moveFocus(FocusDirection.Down)
                    }
                ),
                textStyle = TextStyle(
                    color = MaterialTheme.colors.onSurface,
                    textDecoration = TextDecoration.None
                ),
                singleLine = true
            )

            OutlinedTextField(
                value = passwordState.value,
                onValueChange = { passwordState.value = it },
                modifier = Modifier.padding(all = SMALL_PADDING),
                label = {
                    Text(text = stringResource(R.string.password_label))
                },
                colors = TextFieldDefaults.textFieldColors(),
                visualTransformation = PasswordVisualTransformation(),
                trailingIcon = {
                    Icon(
                        imageVector = Icons.Default.Lock,
                        contentDescription = null,
                        tint = MaterialTheme.colors.onSurface
                    )
                },
                keyboardOptions = KeyboardOptions(
                    imeAction = ImeAction.Done
                ),
                keyboardActions = KeyboardActions(
                    onDone = {
                        keyboardController?.hide()
                        focusManager.clearFocus()
                    }
                ),
                textStyle = TextStyle(
                    color = MaterialTheme.colors.onSurface,
                    textDecoration = TextDecoration.None
                ),
                singleLine = true
            )

            Button(
                onClick = {
                    mainViewModel.onLogin(
                        email = emailState.value.text.trim(),
                        password = passwordState.value.text.trim()
                    )
                },
                modifier = Modifier.padding(all = SMALL_PADDING)
            ) {
                Text(
                    text = stringResource(id = R.string.login_title),
                    color = MaterialTheme.colors.onSurface
                )
            }

            Text(
                text = stringResource(R.string.new_user_title),
                color = Color.Blue,
                modifier = Modifier
                    .padding(all = SMALL_PADDING)
                    .clickable {
                        navigateTo(
                            navController = navController,
                            screen = NavScreens.SignUp
                        )
                    }
            )
        }

        if (isLoading) {
            CommonProgressSpinner()
        }
    }
}