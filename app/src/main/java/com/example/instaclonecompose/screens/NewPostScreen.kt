package com.example.instaclonecompose.screens

import android.net.Uri
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.defaultMinSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.MaterialTheme
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.material.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.style.TextDecoration
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import coil.compose.rememberImagePainter
import com.example.instaclonecompose.R
import com.example.instaclonecompose.ui.theme.MEDIUM_PADDING
import com.example.instaclonecompose.ui.theme.SMALL_PADDING
import com.example.instaclonecompose.ui.theme.TEXT_FIELD_HEIGHT
import com.example.instaclonecompose.utils.CommonDivider
import com.example.instaclonecompose.utils.CommonProgressSpinner
import com.example.instaclonecompose.viewmodel.MainViewModel

@ExperimentalComposeUiApi
@Composable
fun NewPostScreen(
    mainViewModel: MainViewModel = hiltViewModel(),
    navController: NavController,
    encodedUri: String
) {
    val inProgress = mainViewModel.inProgress.value

    val imageUri by remember { mutableStateOf(encodedUri) }

    var description by rememberSaveable { mutableStateOf("") }

    val scrollState = rememberScrollState()

    val keyboardController = LocalSoftwareKeyboardController.current

    val focusManager = LocalFocusManager.current

    Column(
        modifier = Modifier
            .verticalScroll(scrollState)
            .fillMaxWidth()
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(all = SMALL_PADDING),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Text(
                text = stringResource(R.string.cancel_title),
                modifier = Modifier.clickable { navController.popBackStack() },
                color = MaterialTheme.colors.onSurface
            )

            Text(
                text = stringResource(R.string.post_title),
                modifier = Modifier.clickable {
                    mainViewModel.onNewPost(
                        uri = Uri.parse(imageUri),
                        description = description
                    ) {
                        navController.popBackStack()
                    }
                    keyboardController?.hide()
                    focusManager.clearFocus()
                },
                color = MaterialTheme.colors.onSurface
            )
        }

        CommonDivider()

        Image(
            painter = rememberImagePainter(data = imageUri),
            contentDescription = null,
            modifier = Modifier
                .fillMaxWidth()
                .defaultMinSize(minHeight = TEXT_FIELD_HEIGHT),
            contentScale = ContentScale.FillWidth
        )

        Row(modifier = Modifier.padding(all = MEDIUM_PADDING)) {

            OutlinedTextField(
                value = description,
                onValueChange = { description = it },
                modifier = Modifier
                    .fillMaxWidth()
                    .height(TEXT_FIELD_HEIGHT),
                label = {
                    Text(
                        text = stringResource(R.string.description_title),
                        color = MaterialTheme.colors.onSurface
                    )
                },
                textStyle = TextStyle(
                    color = MaterialTheme.colors.onSurface,
                    textDecoration = TextDecoration.None
                ),
                singleLine = false,
                colors = TextFieldDefaults.textFieldColors(
                    backgroundColor = Color.Transparent,
                ),
                keyboardOptions = KeyboardOptions(
                    imeAction = ImeAction.Done
                ),
                keyboardActions = KeyboardActions(
                    onDone = {
                        keyboardController?.hide()
                        focusManager.clearFocus()
                    }
                )
            )
        }
    }

    if (inProgress) {
        CommonProgressSpinner()
    }
}