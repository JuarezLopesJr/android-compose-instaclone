package com.example.instaclonecompose.screens

import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Search
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextDecoration
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import coil.annotation.ExperimentalCoilApi
import com.example.instaclonecompose.R
import com.example.instaclonecompose.model.NavParams
import com.example.instaclonecompose.navigation.NavScreens
import com.example.instaclonecompose.ui.theme.DIVIDER_WIDTH
import com.example.instaclonecompose.ui.theme.SMALL_PADDING
import com.example.instaclonecompose.utils.Constants.POSTS
import com.example.instaclonecompose.utils.navigateTo
import com.example.instaclonecompose.viewmodel.MainViewModel

@ExperimentalComposeUiApi
@ExperimentalCoilApi
@Composable
fun SearchScreen(
    navController: NavController,
    mainViewModel: MainViewModel = hiltViewModel()
) {
    val searchedPostsLoading = mainViewModel.searchedPostsProgress.value
    val searchedPosts = mainViewModel.searchedPosts.value
    var searchTerm by rememberSaveable { mutableStateOf("") }

    Column(modifier = Modifier.fillMaxSize()) {
        SearchBar(
            searchTerm = searchTerm,
            onSearchChange = { searchTerm = it },
            onSearch = { mainViewModel.searchPosts(searchTerm) },
            onCloseClick = {
                if (searchTerm.isNotEmpty()) {
                    searchTerm = ""
                    mainViewModel.searchedPosts.value = emptyList()
                } else {
                    navController.popBackStack()
                }
            }
        )

        PostList(
            modifier = Modifier
                .weight(1f)
                .fillMaxWidth()
                .padding(all = SMALL_PADDING),
            isContextLoading = false,
            postLoading = searchedPostsLoading,
            posts = searchedPosts,
            onPostClick = {
                navigateTo(
                    navController = navController,
                    screen = NavScreens.SinglePost,
                    NavParams(POSTS, it)
                )
            }
        )

        BottomNavigationMenu(
            selectedItem = BottomNavigationItem.SEARCH,
            navController = navController
        )
    }
}

@ExperimentalComposeUiApi
@Composable
fun SearchBar(
    searchTerm: String,
    onSearchChange: (String) -> Unit,
    onSearch: () -> Unit,
    onCloseClick: () -> Unit
) {
    val keyboardController = LocalSoftwareKeyboardController.current
    val focusManager = LocalFocusManager.current

    TextField(
        value = searchTerm,
        onValueChange = onSearchChange,
        modifier = Modifier
            .padding(all = SMALL_PADDING)
            .fillMaxWidth()
            .border(DIVIDER_WIDTH, Color.LightGray, CircleShape),
        shape = CircleShape,
        placeholder = {
            Text(
                text = stringResource(R.string.search_post_title),
                color = MaterialTheme.colors.onSurface
            )
        },
        colors = TextFieldDefaults.textFieldColors(
            backgroundColor = Color.Transparent,
            focusedIndicatorColor = Color.Transparent,
            unfocusedIndicatorColor = Color.Transparent,
            disabledIndicatorColor = Color.Transparent
        ),
        leadingIcon = {
            Icon(
                imageVector = Icons.Default.Search,
                contentDescription = null,
                tint = MaterialTheme.colors.onSurface
            )
        },
        trailingIcon = {
            IconButton(
                onClick = { onCloseClick.invoke() }
            ) {
                Icon(
                    imageVector = Icons.Default.Close,
                    contentDescription = null,
                    tint = MaterialTheme.colors.onSurface
                )
            }
        },
        keyboardOptions = KeyboardOptions(
            keyboardType = KeyboardType.Text,
            imeAction = ImeAction.Search
        ),
        keyboardActions = KeyboardActions(
            onSearch = {
                onSearch.invoke()
                keyboardController?.hide()
                focusManager.clearFocus()
            }
        ),
        textStyle = TextStyle(
            color = MaterialTheme.colors.onSurface,
            textDecoration = TextDecoration.None
        ),
        singleLine = true
    )
}

/*
@ExperimentalComposeUiApi
@Preview
@Composable
fun SearchPreview() {
    SearchBar(
        navController = rememberNavController(),
        searchTerm = "",
        onSearchChange = {},
        onSearch = {}
    )
}*/
