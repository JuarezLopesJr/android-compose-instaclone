package com.example.instaclonecompose.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.material.MaterialTheme
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.Person
import androidx.compose.material.icons.filled.Search
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.navigation.NavController
import com.example.instaclonecompose.navigation.NavScreens
import com.example.instaclonecompose.ui.theme.BOTTOM_ICON_SIZE
import com.example.instaclonecompose.ui.theme.EXTRA_SMALL_PADDING
import com.example.instaclonecompose.utils.navigateTo

enum class BottomNavigationItem(val icon: ImageVector, val screen: NavScreens) {

    FEED(icon = Icons.Default.Home, screen = NavScreens.Feed),

    SEARCH(icon = Icons.Default.Search, screen = NavScreens.Search),

    POSTS(icon = Icons.Default.Person, screen = NavScreens.MyPosts)
}

@Composable
fun BottomNavigationMenu(
    selectedItem: BottomNavigationItem,
    navController: NavController
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .wrapContentHeight()
            .padding(top = EXTRA_SMALL_PADDING)
            .background(MaterialTheme.colors.onPrimary),
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        for (item in BottomNavigationItem.values()) {
            Image(
                imageVector = item.icon,
                contentDescription = null,
                modifier = Modifier
                    .size(BOTTOM_ICON_SIZE)
                    .padding(EXTRA_SMALL_PADDING)
                    .weight(1f)
                    .clickable {
                        navigateTo(navController = navController, screen = item.screen)
                    },
                colorFilter = if (item == selectedItem) {
                    ColorFilter.tint(Color.Green)
                } else {
                    ColorFilter.tint(Color.Gray)
                }
            )
        }
    }
}

/*
@Preview
@Composable
fun BottomPreview() {
    BottomNavigationMenu(
        selectedItem = BottomNavigationItem.FEED,
        navController = rememberNavController()
    )
}*/
