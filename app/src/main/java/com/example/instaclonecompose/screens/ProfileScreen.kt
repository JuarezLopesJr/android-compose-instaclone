package com.example.instaclonecompose.screens

import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.style.TextDecoration
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import coil.annotation.ExperimentalCoilApi
import com.example.instaclonecompose.R
import com.example.instaclonecompose.navigation.NavScreens
import com.example.instaclonecompose.ui.theme.EXTRA_SMALL_PADDING
import com.example.instaclonecompose.ui.theme.MEDIUM_PADDING
import com.example.instaclonecompose.ui.theme.ROW_WIDTH
import com.example.instaclonecompose.ui.theme.SMALL_PADDING
import com.example.instaclonecompose.ui.theme.TEXT_FIELD_HEIGHT
import com.example.instaclonecompose.utils.CommonDivider
import com.example.instaclonecompose.utils.CommonImage
import com.example.instaclonecompose.utils.CommonProgressSpinner
import com.example.instaclonecompose.utils.navigateTo
import com.example.instaclonecompose.viewmodel.MainViewModel

@ExperimentalCoilApi
@ExperimentalComposeUiApi
@Composable
fun ProfileScreen(
    navController: NavController,
    mainViewModel: MainViewModel = hiltViewModel()
) {
    val isLoading = mainViewModel.inProgress.value

    if (isLoading) {
        CommonProgressSpinner()
    } else {
        /* these values MUST BE HERE in oder to pre-populate the EditProfile screen values */
        val userData = mainViewModel.userData.value

        var name by rememberSaveable {
            mutableStateOf(userData?.name ?: "")
        }

        var userName by rememberSaveable {
            mutableStateOf(userData?.userName ?: "")
        }

        var bio by rememberSaveable {
            mutableStateOf(userData?.bio ?: "")
        }

        ProfileContent(
            mainViewModel = mainViewModel,
            name = name,
            userName = userName,
            bio = bio,
            onNameChange = { name = it },
            onUsernameChange = { userName = it },
            onBioChange = { bio = it },
            onSave = {
                mainViewModel
                    .updateProfileData(
                        name = name,
                        userName = userName,
                        bio = bio
                    )
            },
            onBack = {
                navigateTo(
                    navController = navController,
                    screen = NavScreens.MyPosts
                )
            },
            // TODO("BUG: edit profile still in the back button when pressed")
            onLogout = {
                mainViewModel.onLogout()
                navigateTo(
                    navController = navController,
                    screen = NavScreens.Login
                )
            }
        )
    }
}

@ExperimentalCoilApi
@ExperimentalComposeUiApi
@Composable
fun ProfileContent(
    mainViewModel: MainViewModel,
    name: String,
    userName: String,
    bio: String,
    onNameChange: (String) -> Unit,
    onUsernameChange: (String) -> Unit,
    onBioChange: (String) -> Unit,
    onSave: () -> Unit,
    onBack: () -> Unit,
    onLogout: () -> Unit,
) {
    val scrollState = rememberScrollState()

    val imageUrl = mainViewModel.userData.value?.imageUrl

    val keyboardController = LocalSoftwareKeyboardController.current

    val focusManager = LocalFocusManager.current

    Column(
        modifier = Modifier
            .verticalScroll(scrollState)
            .padding(all = SMALL_PADDING)
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(all = SMALL_PADDING),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Text(
                text = stringResource(R.string.back_title),
                modifier = Modifier.clickable { onBack.invoke() },
                color = MaterialTheme.colors.onSurface
            )

            Text(
                text = stringResource(R.string.save_title),
                modifier = Modifier.clickable { onSave.invoke() },
                color = MaterialTheme.colors.onSurface
            )

        }

        CommonDivider()

        ProfileImage(imageUrl = imageUrl, mainViewModel = mainViewModel)

        CommonDivider()

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = EXTRA_SMALL_PADDING, end = EXTRA_SMALL_PADDING),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                text = stringResource(R.string.profile_name_title),
                modifier = Modifier.width(ROW_WIDTH),
                color = MaterialTheme.colors.onSurface
            )

            TextField(
                value = name,
                onValueChange = onNameChange,
                colors = TextFieldDefaults.textFieldColors(
                    backgroundColor = Color.Transparent
                ),
                textStyle = TextStyle(
                    color = MaterialTheme.colors.onSurface,
                    textDecoration = TextDecoration.None
                ),
                keyboardOptions = KeyboardOptions(
                    imeAction = ImeAction.Next
                ),
                keyboardActions = KeyboardActions(
                    onNext = {
                        focusManager.moveFocus(FocusDirection.Down)
                    }
                )
            )
        }

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = EXTRA_SMALL_PADDING, end = EXTRA_SMALL_PADDING),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                text = stringResource(R.string.profile_username_title),
                modifier = Modifier.width(ROW_WIDTH),
                color = MaterialTheme.colors.onSurface
            )

            TextField(
                value = userName,
                onValueChange = onUsernameChange,
                colors = TextFieldDefaults.textFieldColors(
                    backgroundColor = Color.Transparent
                ),
                textStyle = TextStyle(
                    color = MaterialTheme.colors.onSurface,
                    textDecoration = TextDecoration.None
                ),
                keyboardOptions = KeyboardOptions(
                    imeAction = ImeAction.Next
                ),
                keyboardActions = KeyboardActions(
                    onNext = {
                        focusManager.moveFocus(FocusDirection.Down)
                    }
                )
            )
        }

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = EXTRA_SMALL_PADDING, end = EXTRA_SMALL_PADDING),
            verticalAlignment = Alignment.Top
        ) {
            Text(
                text = stringResource(R.string.profile_bio_title),
                modifier = Modifier.width(ROW_WIDTH),
                color = MaterialTheme.colors.onSurface
            )

            TextField(
                value = bio,
                onValueChange = onBioChange,
                colors = TextFieldDefaults.textFieldColors(
                    backgroundColor = Color.Transparent
                ),
                textStyle = TextStyle(
                    color = MaterialTheme.colors.onSurface,
                    textDecoration = TextDecoration.None
                ),
                singleLine = false,
                modifier = Modifier.height(TEXT_FIELD_HEIGHT),
                keyboardOptions = KeyboardOptions(
                    imeAction = ImeAction.Done
                ),
                keyboardActions = KeyboardActions(
                    onDone = {
                        keyboardController?.hide()
                    }
                )
            )
        }

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = MEDIUM_PADDING, bottom = MEDIUM_PADDING),
            horizontalArrangement = Arrangement.Center
        ) {
            Text(
                text = stringResource(R.string.logout_title),
                modifier = Modifier.clickable { onLogout.invoke() },
                color = MaterialTheme.colors.onSurface
            )
        }
    }
}

/* IntrinsicSize.Min means that it'll have a height even when the image is loading */
@ExperimentalCoilApi
@Composable
fun ProfileImage(
    imageUrl: String?,
    mainViewModel: MainViewModel
) {
    val launcher =
        rememberLauncherForActivityResult(
            contract = ActivityResultContracts.GetContent()
        ) { uri ->
            uri?.let {
                mainViewModel.uploadProfileImage(it)
            }
        }

    Box(modifier = Modifier.height(IntrinsicSize.Min)) {
        val isLoading = mainViewModel.inProgress.value

        if (isLoading) {
            CommonProgressSpinner()
        }

        Column(
            modifier = Modifier
                .padding(all = SMALL_PADDING)
                .fillMaxWidth()
                .clickable {
                    launcher.launch("image/*")
                },
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Card(
                shape = CircleShape, modifier = Modifier
                    .padding(all = SMALL_PADDING)
                    .size(ROW_WIDTH)
            ) {
                CommonImage(data = imageUrl)
            }

            Text(
                text = stringResource(R.string.change_profile_title),
                color = MaterialTheme.colors.onSurface
            )
        }
    }
}
