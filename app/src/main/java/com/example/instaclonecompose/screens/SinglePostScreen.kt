package com.example.instaclonecompose.screens

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.defaultMinSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Card
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.outlined.FavoriteBorder
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import coil.annotation.ExperimentalCoilApi
import com.example.instaclonecompose.R
import com.example.instaclonecompose.model.PostData
import com.example.instaclonecompose.navigation.NavScreens
import com.example.instaclonecompose.ui.theme.BOX_HEIGHT
import com.example.instaclonecompose.ui.theme.CARD_DEFAULT_SIZE
import com.example.instaclonecompose.ui.theme.ICON_SIZE
import com.example.instaclonecompose.ui.theme.SMALL_PADDING
import com.example.instaclonecompose.ui.theme.TEXT_FIELD_HEIGHT
import com.example.instaclonecompose.ui.theme.ZERO_PADDING
import com.example.instaclonecompose.utils.CommonDivider
import com.example.instaclonecompose.utils.CommonImage
import com.example.instaclonecompose.viewmodel.MainViewModel

@ExperimentalCoilApi
@Composable
fun SinglePostScreen(
    mainViewModel: MainViewModel = hiltViewModel(),
    navController: NavController,
    post: PostData
) {
    val comments = mainViewModel.comments.value

    LaunchedEffect(key1 = Unit) {
        mainViewModel.getComments(post.postId)
    }

    post.userId?.let {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentHeight()
                .padding(all = SMALL_PADDING)
        ) {
            Text(
                text = stringResource(id = R.string.back_title),
                color = MaterialTheme.colors.onSurface,
                modifier = Modifier.clickable {
                    navController.popBackStack()
                }
            )

            CommonDivider()

            SinglePostDisplay(
                mainViewModel = mainViewModel,
                navController = navController,
                post = post,
                qtComments = comments.size
            )
        }
    }
}

@ExperimentalCoilApi
@Composable
fun SinglePostDisplay(
    mainViewModel: MainViewModel,
    navController: NavController,
    post: PostData,
    qtComments: Int
) {
    val userData = mainViewModel.userData.value

    Box(
        modifier = Modifier
            .fillMaxWidth()
            .height(BOX_HEIGHT)
    ) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Card(
                shape = CircleShape, modifier = Modifier
                    .padding(all = SMALL_PADDING)
                    .size(CARD_DEFAULT_SIZE)
            ) {
                CommonImage(data = post.userImage)
            }

            Text(
                text = post.userName ?: "",
                color = MaterialTheme.colors.onSurface
            )

            Text(
                text = ".",
                modifier = Modifier.padding(all = SMALL_PADDING),
                color = MaterialTheme.colors.onSurface
            )
            when {
                /* current user's post don't show anything */
                userData?.userId == post.userId -> {}

                userData?.following?.contains(post.userId) == true -> {
                    Text(
                        text = stringResource(R.string.unfollow_title),
                        color = Color.Red,
                        modifier = Modifier.clickable {
                            mainViewModel.onFollowClick(post.userId!!)
                        }
                    )
                }

                else -> {
                    Text(
                        text = stringResource(R.string.follow_title),
                        color = Color.Blue,
                        modifier = Modifier.clickable {
                            mainViewModel.onFollowClick(post.userId!!)
                        }
                    )
                }
            }
        }
    }

    Box {
        val imageModifier = Modifier
            .fillMaxWidth()
            .defaultMinSize(minHeight = TEXT_FIELD_HEIGHT)

        CommonImage(
            data = post.postImage,
            modifier = imageModifier,
            contentScale = ContentScale.FillWidth
        )
    }

    Row(
        modifier = Modifier.padding(SMALL_PADDING),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Icon(
            imageVector = if (post.likes.isNullOrEmpty())
                Icons.Outlined.FavoriteBorder else Icons.Default.Favorite,
            contentDescription = null,
            tint = if (post.likes.isNullOrEmpty()) Color.LightGray else Color.Red,
            modifier = Modifier.size(ICON_SIZE)
        )

        Text(
            text = " ${post.likes?.size ?: 0} likes",
            color = MaterialTheme.colors.onSurface,
            modifier = Modifier.padding(start = ZERO_PADDING)
        )
    }

    Row(modifier = Modifier.padding(SMALL_PADDING)) {
        Text(
            text = post.userName ?: "",
            fontWeight = FontWeight.Bold,
            color = MaterialTheme.colors.onSurface
        )

        Text(
            text = post.postDescription ?: "",
            modifier = Modifier.padding(start = SMALL_PADDING),
            color = MaterialTheme.colors.onSurface
        )
    }

    Row(
        modifier = Modifier
            .padding(all = SMALL_PADDING)
            .clickable {
                post.postId?.let {
                    navController.navigate(NavScreens.Comments.createRoute(it))
                }
            }
    ) {
        Text(
            text = "$qtComments comments",
            color = Color.Gray,
            modifier = Modifier.padding(start = SMALL_PADDING)
        )
    }
}
