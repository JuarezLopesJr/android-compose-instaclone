package com.example.instaclonecompose.screens

import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.defaultMinSize
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.layout.ContentScale
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import coil.annotation.ExperimentalCoilApi
import com.example.instaclonecompose.model.NavParams
import com.example.instaclonecompose.model.PostData
import com.example.instaclonecompose.navigation.NavScreens
import com.example.instaclonecompose.ui.theme.BOX_HEIGHT
import com.example.instaclonecompose.ui.theme.CARD_DEFAULT_SIZE
import com.example.instaclonecompose.ui.theme.EXTRA_SMALL_PADDING
import com.example.instaclonecompose.ui.theme.TEXT_FIELD_HEIGHT
import com.example.instaclonecompose.utils.CommonImage
import com.example.instaclonecompose.utils.CommonProgressSpinner
import com.example.instaclonecompose.utils.Constants.POSTS
import com.example.instaclonecompose.utils.LikeAnimation
import com.example.instaclonecompose.utils.UserImageCard
import com.example.instaclonecompose.utils.navigateTo
import com.example.instaclonecompose.viewmodel.MainViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@ExperimentalCoilApi
@Composable
fun FeedScreen(
    navController: NavController,
    mainViewModel: MainViewModel = hiltViewModel()
) {
    val userDataLoading = mainViewModel.inProgress.value
    val userData = mainViewModel.userData.value
    val personalizedFeed = mainViewModel.postFeed.value
    val personalizedFeedLoading = mainViewModel.postFeedProgress.value

    Column(
        modifier = Modifier
            .fillMaxSize()
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentHeight()
        ) {
            UserImageCard(userImage = userData?.imageUrl)
        }
        PostsList(
            posts = personalizedFeed,
            modifier = Modifier.weight(1f),
            loading = personalizedFeedLoading or userDataLoading,
            navController = navController,
            mainViewModel = mainViewModel,
            currentUserId = userData?.userId ?: ""
        )

        BottomNavigationMenu(
            selectedItem = BottomNavigationItem.FEED,
            navController = navController
        )
    }

}

@ExperimentalCoilApi
@Composable
fun PostsList(
    modifier: Modifier = Modifier,
    posts: List<PostData>,
    loading: Boolean,
    navController: NavController,
    mainViewModel: MainViewModel,
    currentUserId: String
) {
    Box(modifier = modifier) {
        LazyColumn {
            items(items = posts) {
                Post(post = it, currentUserId = currentUserId, mainViewModel) {
                    navigateTo(
                        navController,
                        NavScreens.SinglePost,
                        NavParams(POSTS, it)
                    )
                }
            }
        }
        if (loading)
            CommonProgressSpinner()
    }
}

@ExperimentalCoilApi
@Composable
fun Post(
    post: PostData,
    currentUserId: String,
    mainViewModel: MainViewModel,
    onPostClick: () -> Unit
) {

    val likeAnimation = remember { mutableStateOf(false) }
    val dislikeAnimation = remember { mutableStateOf(false) }

    Card(
        shape = RoundedCornerShape(corner = CornerSize(EXTRA_SMALL_PADDING)),
        modifier = Modifier
            .fillMaxWidth()
            .wrapContentHeight()
            .padding(top = EXTRA_SMALL_PADDING, bottom = EXTRA_SMALL_PADDING)
    ) {
        Column {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(BOX_HEIGHT),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Card(
                    shape = CircleShape, modifier = Modifier
                        .padding(EXTRA_SMALL_PADDING)
                        .size(CARD_DEFAULT_SIZE)
                ) {
                    CommonImage(
                        data = post.userImage,
                        contentScale = ContentScale.Crop
                    )
                }
                Text(
                    text = post.userName ?: "",
                    modifier = Modifier.padding(all = EXTRA_SMALL_PADDING),
                    color = MaterialTheme.colors.onSurface
                )
            }

            Box(
                modifier = Modifier.fillMaxWidth(),
                contentAlignment = Alignment.Center
            ) {
                val modifier = Modifier
                    .fillMaxWidth()
                    .defaultMinSize(minHeight = TEXT_FIELD_HEIGHT)
                    .pointerInput(Unit) {
                        detectTapGestures(
                            onDoubleTap = {
                                if (post.likes?.contains(currentUserId) == true) {
                                    dislikeAnimation.value = true
                                } else {
                                    likeAnimation.value = true
                                }
                                mainViewModel.onLikePost(post)
                            },
                            onTap = {
                                onPostClick.invoke()
                            }
                        )
                    }
                CommonImage(
                    data = post.postImage,
                    modifier = modifier,
                    contentScale = ContentScale.FillWidth
                )

                if (likeAnimation.value) {
                    CoroutineScope(Dispatchers.Main).launch {
                        delay(1000L)
                        likeAnimation.value = false
                    }
                    LikeAnimation()
                }
                if (dislikeAnimation.value) {
                    CoroutineScope(Dispatchers.Main).launch {
                        delay(1000L)
                        dislikeAnimation.value = false
                    }
                    LikeAnimation(false)
                }
            }
        }
    }
}