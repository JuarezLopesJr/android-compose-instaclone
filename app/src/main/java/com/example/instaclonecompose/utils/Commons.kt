package com.example.instaclonecompose.utils

import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.activity.OnBackPressedDispatcher
import androidx.activity.compose.LocalOnBackPressedDispatcherOwner
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.animateDp
import androidx.compose.animation.core.spring
import androidx.compose.animation.core.updateTransition
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Card
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Divider
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.outlined.FavoriteBorder
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberUpdatedState
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import coil.annotation.ExperimentalCoilApi
import coil.compose.ImagePainter
import coil.compose.rememberImagePainter
import com.example.instaclonecompose.R
import com.example.instaclonecompose.model.NavParams
import com.example.instaclonecompose.navigation.NavScreens
import com.example.instaclonecompose.ui.theme.CARD_IMAGE_SIZE
import com.example.instaclonecompose.ui.theme.DIVIDER_WIDTH
import com.example.instaclonecompose.ui.theme.SMALL_PADDING
import com.example.instaclonecompose.ui.theme.TEXT_FIELD_HEIGHT
import com.example.instaclonecompose.ui.theme.ZERO_PADDING
import com.example.instaclonecompose.viewmodel.MainViewModel

@Composable
fun NotificationMessage(mainViewModel: MainViewModel = hiltViewModel()) {
    val notificationState = mainViewModel.popupNotification.value
    val notificationMessage = notificationState?.getContentOrNull()

    if (notificationMessage != null) {
        Toast.makeText(LocalContext.current, notificationMessage, Toast.LENGTH_SHORT).show()
    }
}

@Composable
fun CommonProgressSpinner() {
    Row(
        modifier = Modifier
            .alpha(alpha = 0.5f)
            .background(Color.LightGray)
            .clickable(enabled = false) {}
            .fillMaxSize(),
        horizontalArrangement = Arrangement.Center,
        verticalAlignment = Alignment.CenterVertically
    ) {
        CircularProgressIndicator()
    }
}

/* function to avoid cyclic navigation in the back stack and passing Parcelable data
    vararg ara optional */
fun navigateTo(
    navController: NavController,
    screen: NavScreens,
    vararg params: NavParams
) {
    for (param in params) {
        navController
            .currentBackStackEntry?.arguments?.putParcelable(param.name, param.value)
    }

    navController.navigate(screen.route) {
        popUpTo(screen.route)
        launchSingleTop = true
    }
}

@Composable
fun CheckSignedIn(
    navController: NavController,
    mainViewModel: MainViewModel
) {
    val alreadyLoggedIn = remember { mutableStateOf(false) }

    val signedIn = mainViewModel.signedIn.value

    if (signedIn && !alreadyLoggedIn.value) {
        alreadyLoggedIn.value = true
        navController.navigate(route = NavScreens.Feed.route) {
            popUpTo(0) // remove all from the backstack leaving only FeedScreen
        }
    }
}

@ExperimentalCoilApi
@Composable
fun CommonImage(
    modifier: Modifier = Modifier,
    data: String?,
    contentScale: ContentScale = ContentScale.Crop
) {
    val painter = rememberImagePainter(data = data)

    Image(
        painter = painter,
        contentDescription = null,
        modifier = modifier.wrapContentSize(),
        contentScale = contentScale
    )

    if (painter.state is ImagePainter.State.Loading) {
        CommonProgressSpinner()
    }
}

@ExperimentalCoilApi
@Composable
fun UserImageCard(
    modifier: Modifier = Modifier,
    userImage: String?
) {
    Card(
        shape = CircleShape, modifier = modifier
            .padding(SMALL_PADDING)
            .size(CARD_IMAGE_SIZE)
    ) {
        if (userImage.isNullOrEmpty()) {
            Image(
                painter = painterResource(id = R.drawable.ic_launcher_foreground),
                contentDescription = null,
                colorFilter = ColorFilter.tint(Color.Gray)
            )
        } else {
            CommonImage(data = userImage)
        }
    }
}

private enum class LikeIconSize {
    SMALL,
    LARGE
}

@Composable
fun LikeAnimation(like: Boolean = true) {
    var sizeState by remember { mutableStateOf(LikeIconSize.SMALL) }
    val transition = updateTransition(targetState = sizeState, label = "")
    val size by transition.animateDp(
        label = "",
        transitionSpec = {
            spring(
                dampingRatio = Spring.DampingRatioMediumBouncy,
                stiffness = Spring.StiffnessLow
            )
        }
    ) { state ->
        when (state) {
            LikeIconSize.SMALL -> ZERO_PADDING
            LikeIconSize.LARGE -> TEXT_FIELD_HEIGHT
        }
    }

    Image(
        imageVector = if (like)
            Icons.Default.Favorite else Icons.Outlined.FavoriteBorder,
        contentDescription = null,
        modifier = Modifier.size(size = size),
        colorFilter = ColorFilter.tint(if (like) Color.Red else Color.Gray)
    )
    sizeState = LikeIconSize.LARGE
}

@Composable
fun CommonDivider() {
    Divider(
        color = Color.LightGray,
        thickness = DIVIDER_WIDTH,
        modifier = Modifier
            .alpha(alpha = 0.3f)
            .padding(top = SMALL_PADDING, bottom = SMALL_PADDING)
    )
}

@Composable
fun BackButtonHandler(
    backPressedDispatcher: OnBackPressedDispatcher? =
        LocalOnBackPressedDispatcherOwner.current?.onBackPressedDispatcher,
    onBackPressed: () -> Unit = {}
) {
    val currentOnBackPressed by rememberUpdatedState(newValue = onBackPressed)

    val backCallback = remember {
        object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                currentOnBackPressed()
            }
        }
    }

    DisposableEffect(key1 = backPressedDispatcher) {
        backPressedDispatcher?.addCallback(backCallback)

        onDispose {
            backCallback.remove()
        }
    }
}