package com.example.instaclonecompose.utils

object Constants {
    // Routes
    const val SIGNUP_SCREEN = "signup"
    const val LOGIN_SCREEN = "login"
    const val FEED_SCREEN = "feed"
    const val SEARCH_SCREEN = "search"
    const val MY_POSTS_SCREEN = "my_posts"
    const val PROFILE_SCREEN = "profile"
    const val NEW_POST_SCREEN = "new_post"
    const val SINGLE_POST_SCREEN = "single_post"
    const val NEW_POST_ROUTE_KEY = "imageUri"
    const val COMMENTS_SCREEN = "comments"
    const val COMMENT_ROUTE_KEY = "postId"

    // UserData map keys
    const val USER_ID = "userId"
    const val POST_ID = "postId"
    const val NAME = "name"
    const val USER_NAME = "userName"
    const val USER_IMAGE = "userImage"
    const val IMAGE_URL = "imageUrl"
    const val BIO = "bio"
    const val FOLLOWING = "following"
    const val USER_ALREADY_TAKEN = "Username already exists"
    const val SIGNUP_FAILED = "Signup failed"
    const val LOGIN_FAILED = "Login failed"
    const val CANNOT_UPDATE_USER = "Cannot update user"
    const val CANNOT_CREATE_USER = "Cannot create user"
    const val CANNOT_RETRIEVE_USER_DATA = "Cannot retrieve user data"
    const val RETRIEVE_USER_DATA_SUCCESS = "User data retrieved successfully"
    const val ALL_FIELDS_REQUIRED = "All fields are required"
    const val LOGGED_OUT_INFO = "Logged out"
    const val USER_NAME_ERROR = "Error: username unavailable. Unable to create post"
    const val USER_NAME_POST_ERROR = "Error: username unavailable. Unable to refresh post"
    const val POST_ERROR = "Unable to create post"
    const val POST_RETRIEVE_ERROR = "Unable to retrieve posts"
    const val POST_SUCCESS = "Post successfully created "
    const val POSTS_UNAVAILABLE = "No posts available"
    const val SEARCH_TERMS_KEY = "searchTerms"
    const val SEARCH_TERMS_ERROR = "Cannot search posts"
    const val FOLLOWING_ERROR = "Something went wrong, please try again"
    const val FEED_ERROR = "Unable to get feed!"
    const val TIME_KEY = "time"
    const val LIKES_ERROR = "Unable to like post"
    const val COMMENTS_ERROR = "Unable to show comments"

    // Firebase Storage
    const val IMAGE_FOLDER = "images"
    const val USERS = "users"
    const val POSTS = "posts"
    const val LIKES = "likes"
    const val COMMENTS = "comments"
}