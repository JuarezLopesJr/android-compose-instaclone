package com.example.instaclonecompose.viewmodel

import android.net.Uri
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import com.example.instaclonecompose.model.CommentData
import com.example.instaclonecompose.model.PostData
import com.example.instaclonecompose.model.UserData
import com.example.instaclonecompose.utils.Constants.ALL_FIELDS_REQUIRED
import com.example.instaclonecompose.utils.Constants.CANNOT_CREATE_USER
import com.example.instaclonecompose.utils.Constants.CANNOT_RETRIEVE_USER_DATA
import com.example.instaclonecompose.utils.Constants.CANNOT_UPDATE_USER
import com.example.instaclonecompose.utils.Constants.COMMENTS
import com.example.instaclonecompose.utils.Constants.COMMENTS_ERROR
import com.example.instaclonecompose.utils.Constants.FEED_ERROR
import com.example.instaclonecompose.utils.Constants.FOLLOWING
import com.example.instaclonecompose.utils.Constants.FOLLOWING_ERROR
import com.example.instaclonecompose.utils.Constants.IMAGE_FOLDER
import com.example.instaclonecompose.utils.Constants.LIKES
import com.example.instaclonecompose.utils.Constants.LIKES_ERROR
import com.example.instaclonecompose.utils.Constants.LOGGED_OUT_INFO
import com.example.instaclonecompose.utils.Constants.LOGIN_FAILED
import com.example.instaclonecompose.utils.Constants.POSTS
import com.example.instaclonecompose.utils.Constants.POST_ERROR
import com.example.instaclonecompose.utils.Constants.POST_ID
import com.example.instaclonecompose.utils.Constants.POST_RETRIEVE_ERROR
import com.example.instaclonecompose.utils.Constants.POST_SUCCESS
import com.example.instaclonecompose.utils.Constants.SEARCH_TERMS_ERROR
import com.example.instaclonecompose.utils.Constants.SEARCH_TERMS_KEY
import com.example.instaclonecompose.utils.Constants.SIGNUP_FAILED
import com.example.instaclonecompose.utils.Constants.TIME_KEY
import com.example.instaclonecompose.utils.Constants.USERS
import com.example.instaclonecompose.utils.Constants.USER_ALREADY_TAKEN
import com.example.instaclonecompose.utils.Constants.USER_ID
import com.example.instaclonecompose.utils.Constants.USER_IMAGE
import com.example.instaclonecompose.utils.Constants.USER_NAME
import com.example.instaclonecompose.utils.Constants.USER_NAME_ERROR
import com.example.instaclonecompose.utils.Constants.USER_NAME_POST_ERROR
import com.example.instaclonecompose.utils.Event
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.storage.FirebaseStorage
import dagger.hilt.android.lifecycle.HiltViewModel
import java.util.UUID.randomUUID
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val auth: FirebaseAuth,
    private val db: FirebaseFirestore,
    private val storage: FirebaseStorage
) : ViewModel() {

    val popupNotification =
        mutableStateOf<Event<String>?>(null)

    val signedIn = mutableStateOf(false)
    val inProgress = mutableStateOf(false)
    val userData = mutableStateOf<UserData?>(null)

    val refreshPostProgress = mutableStateOf(false)
    val posts = mutableStateOf<List<PostData>>(emptyList())

    val searchedPosts = mutableStateOf<List<PostData>>(emptyList())
    val searchedPostsProgress = mutableStateOf(false)

    val postFeed = mutableStateOf<List<PostData>>(emptyList())
    val postFeedProgress = mutableStateOf(false)

    val comments = mutableStateOf<List<CommentData>>(emptyList())
    val commentProgress = mutableStateOf(false)

    val followers = mutableStateOf(0)

    init {
        val currentUser = auth.currentUser
        signedIn.value = currentUser != null

        currentUser?.uid?.let { uid ->
            getUserData(uid)
        }
    }

    private fun createOrUpdateProfile(
        name: String? = null,
        userName: String? = null,
        imageUrl: String? = null,
        bio: String? = null
    ) {
        val uid = auth.currentUser?.uid

        val userModel = UserData(
            userId = uid,
            name = name ?: userData.value?.name,
            userName = userName ?: userData.value?.userName,
            imageUrl = imageUrl ?: userData.value?.imageUrl,
            bio = bio ?: userData.value?.bio,
            following = userData.value?.following
        )

        uid?.let { id ->
            inProgress.value = true
            db.collection(USERS).document(id)
                .get()
                .addOnSuccessListener { documentSnapshot ->
                    if (documentSnapshot.exists()) {
                        documentSnapshot.reference.update(userModel.toMap())
                            .addOnSuccessListener {
                                this.userData.value = userModel
                                inProgress.value = false
                            }
                            .addOnFailureListener {
                                handleException(it, CANNOT_UPDATE_USER)
                                inProgress.value = false
                            }
                    } else {
                        db.collection(USERS).document(uid).set(userModel)
                        getUserData(uid)
                        inProgress.value = false
                    }
                }
                .addOnFailureListener {
                    handleException(it, CANNOT_CREATE_USER)
                    inProgress.value = false
                }
        }
    }

    private fun getUserData(uid: String) {
        inProgress.value = true
        db.collection(USERS).document(uid).get()
            .addOnSuccessListener {
                val user = it.toObject<UserData>()
                userData.value = user
                inProgress.value = false
                refreshPosts()
                getPersonalizedFeed()
                getFollowers(user?.userId)
            }
            .addOnFailureListener {
                handleException(it, CANNOT_RETRIEVE_USER_DATA)
                inProgress.value = false
            }
    }

    private fun uploadImage(uri: Uri, onSuccess: (Uri) -> Unit) {
        inProgress.value = true

        val storageRef = storage.reference
        val uuid = randomUUID()
        val imageRef = storageRef.child("$IMAGE_FOLDER/$uuid")
        val uploadTask = imageRef.putFile(uri)

        uploadTask.addOnSuccessListener { task ->
            val result = task.metadata?.reference?.downloadUrl

            result?.addOnSuccessListener(onSuccess)
        }
            .addOnFailureListener {
                handleException(exception = it)
                inProgress.value = false
            }
    }

    private fun updatePostUserImageData(imageUrl: String) {
        val currentUid = auth.currentUser?.uid

        db.collection(POSTS).whereEqualTo(USER_ID, currentUid).get()
            .addOnSuccessListener { snapshot ->
                val posts =
                    mutableStateOf<List<PostData>>(arrayListOf())

                convertPosts(documents = snapshot, outState = posts)

                val refs = arrayListOf<DocumentReference>()

                for (post in posts.value) {
                    post.postId?.let {
                        refs.add(db.collection(POSTS).document(it))
                    }
                }

                if (refs.isNotEmpty()) {
                    db.runBatch { batch ->
                        for (ref in refs) {
                            batch.update(ref, USER_IMAGE, imageUrl)
                        }
                    }
                        .addOnSuccessListener {
                            refreshPosts()
                        }
                }
            }
    }

    private fun onCreatePost(
        imageUri: Uri,
        description: String,
        onPostSuccess: () -> Unit
    ) {
        inProgress.value = true
        val currentUid = auth.currentUser?.uid
        val currentUserName = userData.value?.userName
        val currentUserImage = userData.value?.imageUrl

        if (currentUid != null) {
            val postUuid = randomUUID().toString()

            /* list of terms to exclude from the search query, to improve accuracy of results */
            val excludeSearchTerms =
                listOf("the", "be", "to", "is", "of", "and", "or", "a", "in", "it")

            /* combined with the excludeSearchTerms the result will be like this
            * user's post #The beach is very beautiful!
            * searchTerms will be split in to this [beach, very, beautiful] */
            val searchTerms = description
                .split(" ", ".", ",", "?", "!", "#", "@", "*")
                .map { it.lowercase() }
                .filter { it.isNotEmpty() && !excludeSearchTerms.contains(it) }

            val post = PostData(
                postId = postUuid,
                userId = currentUid,
                userName = currentUserName,
                userImage = currentUserImage,
                postImage = imageUri.toString(),
                postDescription = description,
                time = System.currentTimeMillis(),
                likes = emptyList(),
                searchTerms = searchTerms
            )
            /* set() also have this options SetOptions.merge() to handle documents creation in FB
                REMINDER: search about */
            db.collection(POSTS).document(postUuid).set(post)
                .addOnSuccessListener {
                    popupNotification.value = Event(POST_SUCCESS)
                    refreshPosts()
                    onPostSuccess.invoke()
                    inProgress.value = false
                }
                .addOnFailureListener {
                    handleException(exception = it, customMessage = POST_ERROR)
                    inProgress.value = false
                }

        } else {
            handleException(customMessage = USER_NAME_ERROR)
            onLogout()
            inProgress.value = false
        }
    }

    private fun getPersonalizedFeed() {
        val following = userData.value?.following

        if (!following.isNullOrEmpty()) {
            postFeedProgress.value = true

            db.collection(POSTS).whereIn(USER_ID, following).get()
                .addOnSuccessListener {
                    convertPosts(it, postFeed)
                    if (postFeed.value.isEmpty()) {
                        getGeneralFeed()
                    } else {
                        postFeedProgress.value = false
                    }
                }
                .addOnFailureListener {
                    handleException(exception = it, customMessage = FEED_ERROR)
                    postFeedProgress.value = false
                }
        } else {
            getGeneralFeed()
        }
    }

    private fun getGeneralFeed() {
        postFeedProgress.value = true
        val currentTime = System.currentTimeMillis()
        val difference = 24 * 60 * 60 * 1000 // 24 hours in millis

        db.collection(POSTS)
            .whereGreaterThan(TIME_KEY, currentTime - difference)
            .get()
            .addOnSuccessListener {
                convertPosts(it, postFeed)
                postFeedProgress.value = false
            }
            .addOnFailureListener {
                handleException(exception = it, customMessage = FEED_ERROR)
                postFeedProgress.value = false
            }
    }

    private fun getFollowers(uid: String?) {
        db.collection(USERS).whereArrayContains(FOLLOWING, uid ?: "").get()
            .addOnSuccessListener { documents ->
                followers.value = documents.size()
            }
    }

    private fun handleException(exception: Exception? = null, customMessage: String = "") {
        exception?.printStackTrace()
        val errorMsg = exception?.localizedMessage ?: ""

        val message = if (customMessage.isEmpty()) errorMsg else "$customMessage $errorMsg"

        popupNotification.value = Event(message)
    }

    /* sanitizing FB's QuerySnapshot document to display content in the UI */
    private fun convertPosts(
        documents: QuerySnapshot,
        outState: MutableState<List<PostData>>
    ) {
        val newPosts = mutableListOf<PostData>()

        documents.forEach {
            val post = it.toObject<PostData>()
            newPosts.add(post)
        }

        /* sorting that the last post is first in the list */
        val sortedPosts = newPosts.sortedByDescending { it.time }
        outState.value = sortedPosts
    }

    private fun refreshPosts() {
        val currentUid = auth.currentUser?.uid

        if (currentUid != null) {
            refreshPostProgress.value = true

            db.collection(POSTS).whereEqualTo(USER_ID, currentUid).get()
                .addOnSuccessListener {
                    convertPosts(documents = it, outState = posts)
                    refreshPostProgress.value = false
                }
                .addOnFailureListener {
                    handleException(exception = it, customMessage = POST_RETRIEVE_ERROR)
                    refreshPostProgress.value = false
                }
        } else {
            handleException(customMessage = USER_NAME_POST_ERROR)
            onLogout()
            inProgress.value = false
        }
    }

    fun onSignUp(
        userName: String,
        email: String,
        password: String,
    ) {

        if (userName.isEmpty() || email.isEmpty() || password.isEmpty()) {
            handleException(customMessage = ALL_FIELDS_REQUIRED)
            return
        }

        inProgress.value = true

        db.collection(USERS).whereEqualTo(USER_NAME, userName).get()
            .addOnSuccessListener { document ->
                if (document.size() > 0) {
                    handleException(
                        customMessage = USER_ALREADY_TAKEN
                    )
                    inProgress.value = false
                } else {
                    auth.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener { task ->
                            if (task.isSuccessful) {
                                signedIn.value = true

                                createOrUpdateProfile(userName = userName)
                            } else {
                                handleException(
                                    exception = task.exception,
                                    customMessage = SIGNUP_FAILED
                                )
                            }
                            inProgress.value = false
                        }
                }
            }
            .addOnFailureListener { }
    }

    fun onLogin(email: String, password: String) {
        if (email.isEmpty() || password.isEmpty()) {
            handleException(customMessage = ALL_FIELDS_REQUIRED)
            return
        }

        inProgress.value = true

        auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    signedIn.value = true
                    inProgress.value = false
                    auth.currentUser?.uid?.let {
                        getUserData(it)
                    }
                } else {
                    handleException(
                        exception = task.exception,
                        customMessage = LOGIN_FAILED
                    )
                    inProgress.value = false
                }
            }
            .addOnFailureListener {
                handleException(exception = it, customMessage = LOGIN_FAILED)
                inProgress.value = false
            }
    }

    fun updateProfileData(
        name: String,
        userName: String,
        bio: String
    ) {
        createOrUpdateProfile(
            name = name,
            userName = userName,
            bio = bio
        )
    }

    fun uploadProfileImage(uri: Uri) {
        uploadImage(uri = uri) {
            createOrUpdateProfile(imageUrl = it.toString())
            updatePostUserImageData(imageUrl = it.toString())
        }
    }

    fun onLogout() {
        auth.signOut()
        signedIn.value = false
        userData.value = null
        searchedPosts.value = emptyList()
        postFeed.value = emptyList()
        comments.value = emptyList()
        popupNotification.value = Event(LOGGED_OUT_INFO)
    }

    fun onNewPost(
        uri: Uri,
        description: String,
        onPostSuccess: () -> Unit
    ) {
        uploadImage(uri) {
            onCreatePost(
                imageUri = it,
                description = description,
                onPostSuccess = onPostSuccess
            )
        }
    }

    fun searchPosts(searchTerm: String) {
        if (searchTerm.isNotEmpty()) {
            searchedPostsProgress.value = true
            db.collection(POSTS)
                .whereArrayContains(SEARCH_TERMS_KEY, searchTerm.trim().lowercase())
                .get()
                .addOnSuccessListener {
                    convertPosts(it, searchedPosts)
                    searchedPostsProgress.value = false
                }
                .addOnFailureListener {
                    handleException(exception = it, customMessage = SEARCH_TERMS_ERROR)
                    searchedPostsProgress.value = false
                }
        }
    }

    fun onFollowClick(userId: String) {
        auth.currentUser?.uid?.let { currentUser ->
            val following = arrayListOf<String>()

            userData.value?.following?.let {
                following.addAll(it)
            }

            if (following.contains(userId)) {
                following.remove(userId)
            } else {
                following.add(userId)
            }

            db.collection(USERS).document(currentUser).update(FOLLOWING, following)
                .addOnSuccessListener {
                    getUserData(currentUser)
                }
                .addOnFailureListener {
                    handleException(exception = it, customMessage = FOLLOWING_ERROR)
                }
        }
    }

    fun onLikePost(postData: PostData) {
        auth.currentUser?.uid?.let { userId ->
            postData.likes?.let { likes ->
                val newLikes = arrayListOf<String>()
                if (likes.contains(userId)) {
                    newLikes.addAll(likes.filter { userId != it })
                } else {
                    newLikes.addAll(likes)
                    newLikes.add(userId)
                }
                postData.postId?.let { postId ->
                    db.collection(POSTS).document(postId).update(LIKES, newLikes)
                        .addOnSuccessListener {
                            postData.likes = newLikes
                        }
                        .addOnFailureListener {
                            handleException(exception = it, customMessage = LIKES_ERROR)
                        }
                }
            }
        }
    }

    fun createComment(postId: String, text: String) {
        commentProgress.value = true
        userData.value?.userName?.let { userName ->
            val commentId = randomUUID().toString()
            val comment = CommentData(
                commentId = commentId,
                postId = postId,
                userName = userName,
                text = text,
                timeStamp = System.currentTimeMillis()
            )
            db.collection(COMMENTS).document(commentId).set(comment)
                .addOnSuccessListener {
                    getComments(postId)
                }
                .addOnFailureListener {
                    handleException(exception = it, customMessage = COMMENTS_ERROR)
                    commentProgress.value = false
                }
        }
    }

    fun getComments(postId: String?) {
        commentProgress.value = true
        db.collection(COMMENTS).whereEqualTo(POST_ID, postId).get()
            .addOnSuccessListener { documents ->
                val newComments = mutableListOf<CommentData>()
                documents.forEach { doc ->
                    val comment = doc.toObject<CommentData>()
                    newComments.add(comment)
                }
                val sortedComments = newComments.sortedByDescending { it.timeStamp }
                comments.value = sortedComments
                commentProgress.value = false
            }
            .addOnFailureListener { exc ->
                handleException(exception = exc, customMessage = COMMENTS_ERROR)
                commentProgress.value = false
            }
    }
}