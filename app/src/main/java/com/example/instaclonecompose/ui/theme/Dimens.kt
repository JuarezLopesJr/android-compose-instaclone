package com.example.instaclonecompose.ui.theme

import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

val ZERO_PADDING = 0.dp
val MEDIUM_PADDING = 16.dp
val SMALL_PADDING = 8.dp
val EXTRA_SMALL_PADDING = 4.dp
val IMAGE_LARGE_WIDTH = 250.dp
val LARGE_TEXT_SIZE = 30.sp
val BOTTOM_ICON_SIZE = 40.dp
val CARD_IMAGE_SIZE = 64.dp
val CARD_SMALL_BORDER = 2.dp
val CARD_DEFAULT_SIZE = 32.dp
val LARGE_CARD_IMAGE_SIZE = 80.dp
val DIVIDER_WIDTH = 1.dp
val ICON_SIZE = 24.dp
val ROW_WIDTH = 100.dp
val ROW_HEIGHT = 120.dp
val BOX_HEIGHT = 48.dp
val TEXT_FIELD_HEIGHT = 150.dp