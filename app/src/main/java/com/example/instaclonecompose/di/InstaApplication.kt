package com.example.instaclonecompose.di

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class InstaApplication: Application()