package com.example.instaclonecompose.navigation

import com.example.instaclonecompose.utils.Constants.COMMENTS_SCREEN
import com.example.instaclonecompose.utils.Constants.COMMENT_ROUTE_KEY
import com.example.instaclonecompose.utils.Constants.FEED_SCREEN
import com.example.instaclonecompose.utils.Constants.LOGIN_SCREEN
import com.example.instaclonecompose.utils.Constants.MY_POSTS_SCREEN
import com.example.instaclonecompose.utils.Constants.NEW_POST_ROUTE_KEY
import com.example.instaclonecompose.utils.Constants.NEW_POST_SCREEN
import com.example.instaclonecompose.utils.Constants.PROFILE_SCREEN
import com.example.instaclonecompose.utils.Constants.SEARCH_SCREEN
import com.example.instaclonecompose.utils.Constants.SIGNUP_SCREEN
import com.example.instaclonecompose.utils.Constants.SINGLE_POST_SCREEN

sealed class NavScreens(val route: String) {
    object SignUp : NavScreens(route = SIGNUP_SCREEN)

    object Login : NavScreens(route = LOGIN_SCREEN)

    object Feed : NavScreens(route = FEED_SCREEN)

    object Search : NavScreens(route = SEARCH_SCREEN)

    object MyPosts : NavScreens(route = MY_POSTS_SCREEN)

    object Profile : NavScreens(route = PROFILE_SCREEN)

    object NewPost : NavScreens(route = "$NEW_POST_SCREEN/{$NEW_POST_ROUTE_KEY}") {
        fun createRoute(imageUri: String) = "$NEW_POST_SCREEN/$imageUri"
    }

    object SinglePost : NavScreens(route = SINGLE_POST_SCREEN)

    object Comments : NavScreens(route = "$COMMENTS_SCREEN/{$COMMENT_ROUTE_KEY}") {
        fun createRoute(postId: String) = "$COMMENTS_SCREEN/$postId"
    }
}
