package com.example.instaclonecompose.navigation

import androidx.compose.runtime.Composable
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import coil.annotation.ExperimentalCoilApi
import com.example.instaclonecompose.model.PostData
import com.example.instaclonecompose.screens.CommentsScreen
import com.example.instaclonecompose.screens.FeedScreen
import com.example.instaclonecompose.screens.LoginScreen
import com.example.instaclonecompose.screens.MyPostsScreen
import com.example.instaclonecompose.screens.NewPostScreen
import com.example.instaclonecompose.screens.ProfileScreen
import com.example.instaclonecompose.screens.SearchScreen
import com.example.instaclonecompose.screens.SignUpScreen
import com.example.instaclonecompose.screens.SinglePostScreen
import com.example.instaclonecompose.utils.Constants.COMMENT_ROUTE_KEY
import com.example.instaclonecompose.utils.Constants.NEW_POST_ROUTE_KEY
import com.example.instaclonecompose.utils.Constants.POSTS
import com.example.instaclonecompose.utils.NotificationMessage

@ExperimentalCoilApi
@ExperimentalComposeUiApi
@Composable
fun SetupNavGraph(navHostController: NavHostController) {
    NavHost(
        navController = navHostController,
        startDestination = NavScreens.SignUp.route
    ) {
        composable(route = NavScreens.SignUp.route) {
            NotificationMessage()
            SignUpScreen(navController = navHostController)
        }

        composable(route = NavScreens.Login.route) {
            NotificationMessage()
            LoginScreen(navController = navHostController)
        }

        composable(route = NavScreens.Feed.route) {
            NotificationMessage()
            FeedScreen(navController = navHostController)
        }

        composable(route = NavScreens.Search.route) {
            NotificationMessage()
            SearchScreen(navController = navHostController)
        }

        composable(route = NavScreens.MyPosts.route) {
            NotificationMessage()
            MyPostsScreen(navController = navHostController)
        }

        composable(route = NavScreens.Profile.route) {
            NotificationMessage()
            ProfileScreen(navController = navHostController)
        }

        composable(route = NavScreens.NewPost.route) {
            val imageUri = it.arguments?.getString(NEW_POST_ROUTE_KEY)

            imageUri?.let { uri ->
                NotificationMessage()
                NewPostScreen(navController = navHostController, encodedUri = uri)
            }
        }

        composable(route = NavScreens.SinglePost.route) {
            val postData =
                navHostController
                    .previousBackStackEntry?.arguments?.getParcelable<PostData>(POSTS)

            postData?.let {
                NotificationMessage()
                SinglePostScreen(
                    navController = navHostController,
                    post = it
                )
            }
        }

        composable(route = NavScreens.Comments.route) {
            val postId = it.arguments?.getString(COMMENT_ROUTE_KEY)

            postId?.let { id ->
                NotificationMessage()
                CommentsScreen(navController = navHostController, postId = id)
            }
        }
    }
}