package com.example.instaclonecompose.model

data class PostRow(
    var post1: PostData? = null,
    var post2: PostData? = null,
    var post3: PostData? = null,
) {
    fun isFull() = post1 != null && post2 != null && post3 != null

    fun add(post: PostData) {
        when {
            post1 == null -> post1 = post
            post2 == null -> post2 = post
            post3 == null -> post3 = post
        }
    }
}
