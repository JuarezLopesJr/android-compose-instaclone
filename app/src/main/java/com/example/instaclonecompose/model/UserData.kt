package com.example.instaclonecompose.model

import com.example.instaclonecompose.utils.Constants.BIO
import com.example.instaclonecompose.utils.Constants.FOLLOWING
import com.example.instaclonecompose.utils.Constants.IMAGE_URL
import com.example.instaclonecompose.utils.Constants.NAME
import com.example.instaclonecompose.utils.Constants.USER_ID
import com.example.instaclonecompose.utils.Constants.USER_NAME

data class UserData(
    var userId: String? = null,
    var name: String? = null,
    var userName: String? = null,
    var imageUrl: String? = null,
    var bio: String? = null,
    var following: List<String>? = null
) {

    fun toMap() = mapOf(
        USER_ID to userId,
        NAME to name,
        USER_NAME to userName,
        IMAGE_URL to imageUrl,
        BIO to bio,
        FOLLOWING to following
    )
}
