package com.example.instaclonecompose.model

import android.os.Parcelable

data class NavParams(
    val name: String,
    val value: Parcelable
)
